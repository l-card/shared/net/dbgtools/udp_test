#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef _WIN32
    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN
    #endif
    #include <windows.h>
    #include <winsock2.h>

    #include <locale.h>
    #include <conio.h>

    typedef int socklen_t;
    typedef unsigned long in_addr_t;

    typedef UINT64 uint64_t;
    typedef UINT32 uint32_t;
    typedef INT32  int32_t;
#else
    #define closesocket(sock) close(sock)

    #include <fcntl.h>
    #include <sys/socket.h>
    #include <sys/select.h>
    #include <netinet/tcp.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>

    #include <errno.h>
    #include <signal.h>

    typedef int SOCKET;
    #define INVALID_SOCKET -1
    #define SOCKET_ERROR -1
#endif

#include "clock.h"


typedef enum {
    ERR_WSA_INIT            = -1,
    ERR_PARAMETERS          = -2,
    ERR_SOCK_CREATE         = -3,
    ERR_SOCK_BIND           = -4,
    ERR_SEND                = -5,
    ERR_SOCK_SET_BUFLEN     = -6,

    ERR_RECV_SELECT         = -10,
    ERR_RECV_TOUT           = -11,
    ERR_RECV                = -12,
    ERR_RECV_INVALID_SRC    = -13,
    ERR_RECV_UNALIGN_PACKET   = -14
} t_err_codes;


static int f_out = 0;

static const char* f_get_last_err_str(void) {
#ifdef _WIN32
    static char f_err_str[512];
    FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL,
                  GetLastError(), 0,
                  f_err_str, sizeof(f_err_str),
                  NULL);
    return &f_err_str[0];
#else
    return strerror( errno ) ;
#endif
}

static int f_recv_packet(SOCKET s, void* data, int* size,
                         const struct sockaddr_in* addr,  unsigned tout_ms) {
    int err = 0;
    fd_set rset, eset;
    struct timeval timeout = { tout_ms/1000,(tout_ms%1000)*1000 };
    struct sockaddr_in rx_addr;
    socklen_t addr_size = sizeof(rx_addr);
    int sel_res = 0;


    /* Receive data */
    FD_ZERO(&rset);
    FD_SET(s, &rset);
    FD_ZERO(&eset);
    FD_SET(s, &eset);
    sel_res = select((int)s+1, &rset, NULL, &eset, &timeout);
    if ((sel_res < 0) || (FD_ISSET(s, &eset))) {
        fprintf(stderr, "������ ������ ������ ��� ������ select: %s\n", f_get_last_err_str());
        err = ERR_RECV_SELECT;
    } else if (!FD_ISSET(s, &rset)) {
        fprintf(stderr, "�� ������� ��������� ������ ������ �� �������� �������!\n");
        err = ERR_RECV_TOUT;
    } else  {
        int data_len = recvfrom(s, data, *size, 0, (struct sockaddr*) &rx_addr, &addr_size);

        if (SOCKET_ERROR == data_len) {
            err = ERR_RECV;
            fprintf(stderr, "������ ������ ������! %s\n", f_get_last_err_str());
        } else if ((addr->sin_port!=0) && (rx_addr.sin_port!=addr->sin_port)
                 && (rx_addr.sin_addr.s_addr!=addr->sin_addr.s_addr)) {
            err = ERR_RECV_INVALID_SRC;
            printf("������ ����� �� �� ������� ����������!\n");
        }

        if (!err)
            *size = data_len;
    }
    return err;
}

#ifndef _WIN32
/* ���������� ������� ���������� ��� Linux */
static void f_abort_handler(int sig) {
    f_out = 1;
}
#endif


typedef struct {
    t_clock start_time;
    uint64_t bytes;
    uint64_t packets;
    uint32_t packets_out_of_order;
    uint32_t packet_errs;
    double speed;
} t_rx_stats;



 
int main( int argc, char **argv) {
    SOCKET sock;
    const char *start_cmd = "start";
    const char *stop_cmd  = "stop";
    int err = 0;
    int send_back = 0;
    int port = 0;
    in_addr_t addr;
#ifdef _WIN32
    WORD wVersionRequested = MAKEWORD(2,2);
    WSADATA wsaData;
#else
    struct sigaction sa;
    /* � �� Linux ������������� ���� ���������� �� ������ ��������,
       ����� ��������� ���� ��������� */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif

#ifdef _WIN32
    /* ��� ������ ������� ���� � ������� ��� �� Windows*/
    setlocale(LC_ALL, "");
#endif

#ifdef _WIN32
    if (WSAStartup(wVersionRequested, &wsaData)) {
        err = ERR_WSA_INIT;
        fprintf(stderr, "WSA Startup error:%s\n", f_get_last_err_str());
    } else if (wsaData.wVersion!=wVersionRequested){
        WSACleanup();
        fprintf(stderr, "������ WSA �� ������������� �������������!\n");
        err = ERR_WSA_INIT;
    }
#endif
   
    /* �������� ������� ���������� */
    if (argc < 3) {
        fprintf(stderr, "������� ������������ ����������! �������������: udp_test <addr> <port> <mode>\n");
        err = ERR_PARAMETERS;
    }     
    if (!err) {
        addr = inet_addr(argv[1]);
        if (addr==INADDR_NONE) {
            fprintf(stderr, "������� ����� ip-����� ����������!\n");
            err = ERR_PARAMETERS;
        }
    }
    if (!err) {
        port = atoi(argv[2]);
        if ((port <= 0) || (port>= 65536)) {
            fprintf(stderr, "������� ����� ����� UDP �����!\n");
            err = ERR_PARAMETERS;
        }
    }

    if (!err && (argc >= 4)) {
        if (!strcmp(argv[3], "loop")) {
            send_back = 1;
        } else if (!strcmp(argv[3], "tx")) {

        } else {
            fprintf(stderr, "������� ����� ����� (���������: tx, loop)\n");
            err = ERR_PARAMETERS;
        }
    }

    /* �������� ������ ��� ������ */
    if (!err) {
        sock = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP );
        if ( sock == INVALID_SOCKET ) {
            fprintf(stderr, "������ �������� ������: %s\n", f_get_last_err_str());
            return ERR_SOCK_CREATE;
        }
    }

    if (!err) {
        int bytes_sent;
        struct sockaddr_in sa;
        uint32_t buf_size = 10*1024*1024;


        sa.sin_family = PF_INET;
        sa.sin_addr.s_addr = addr;
        sa.sin_port = htons( port );

        if (!err && (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char*)&buf_size, sizeof(buf_size))
                     == SOCKET_ERROR)) {
            fprintf(stderr, "������ ��������� ������� ������ �� �����: %s\n", f_get_last_err_str());
            err = ERR_SOCK_SET_BUFLEN;
        }

        if (!err && (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, (char*)&buf_size, sizeof(buf_size))
                     == SOCKET_ERROR)) {
            fprintf(stderr, "������ ��������� ������� ������ �� ��������: %s\n", f_get_last_err_str());
            err = ERR_SOCK_SET_BUFLEN;
        }

        /* ������� ������ ��� ������� ����� */
        if (!err) {
            bytes_sent = sendto(sock, start_cmd, strlen( start_cmd ) + 1, 0,
                         ( struct sockaddr* )&sa, sizeof( struct sockaddr_in ));

            if ( bytes_sent < 0 ) {
                fprintf(stderr, "������ �������� ������ ������� �����: %s\n", f_get_last_err_str() );
                err = ERR_SEND;
            } else {
                printf("������ ����� ������� �����.\n");
            }
        }

        if (!err) {
            int last_packet_size = 0;
            uint32_t rcv_buf[1500/4];
            uint32_t cur_cntr = 0;
            t_clock cur_clock;
            t_rx_stats start_stat; /* ����� ���������� � ������ ������� */
            t_rx_stats intv_stat;
            uint64_t total_time = 0;
            memset(&start_stat, 0, sizeof(start_stat));
            memset(&intv_stat, 0, sizeof(intv_stat));

            start_stat.start_time = intv_stat.start_time = clock_time();
            printf("          ������� | �������� (����.) | �������� (������.) |  ������ | ��� ��. |\n");


            while (!err && !f_out) {
                int bytes_recvd = sizeof(rcv_buf);

                err =  f_recv_packet(sock, &rcv_buf, &bytes_recvd, &sa, 2000);
                if (!err) {
                    if (bytes_recvd % 4) {
                        fprintf(stderr, "������������� ������ ��������� ������ (%d)\n", bytes_recvd);
                        err = ERR_RECV_UNALIGN_PACKET;
                    } else {
                        int i;
                        t_clock intv_time;
                        start_stat.bytes += bytes_recvd;


                        if (cur_cntr != rcv_buf[0]) {
                            int32_t delta = (int32_t)(rcv_buf[0]-cur_cntr);
                            printf("������ ����� �� � ��������� ������� (��������� = 0x%08X, ������� = 0x%08X)\n",
                                   cur_cntr, rcv_buf[0]);

                            if (last_packet_size && (delta>0))
                                printf("######## �������� %d ���� (%.3f �������) ######\n", 4*delta, (4. * (double)delta)/last_packet_size );
                            else
                                printf("!!!!!!!! ������ %d ���� (%.3f �������) !!!!!\n", -4*delta, (-4. * (double)delta)/last_packet_size );

                            cur_cntr = rcv_buf[0];
                            start_stat.packets_out_of_order++;
                        }


                        for (i=0; i < bytes_recvd/4; i++, cur_cntr++) {
                            if (cur_cntr != rcv_buf[i]) {
                                fprintf(stderr, "������ �������� � �������� ������! (��������� = 0x%08X, ������� = 0x%08X)\n",
                                       cur_cntr, rcv_buf[i]);
                                cur_cntr = rcv_buf[i];
                                start_stat.packet_errs++;
                                break;
                            }
                        }

                        start_stat.packets++;

                        if (send_back) {
                            sendto(sock, (char*)rcv_buf, bytes_recvd, 0,
                               ( struct sockaddr* )&sa, sizeof( struct sockaddr_in ));
                        }




                        cur_clock = clock_time();
                        intv_time = cur_clock - intv_stat.start_time;

                        if (intv_time >= CLOCK_CONF_SECOND) {
                            uint64_t block_bytes = start_stat.bytes - intv_stat.bytes;
                            total_time += intv_time;



                            printf("%11.3f ����� | %8.3f M����/c | %10.3f M����/c | %7u | %7u |\n",
                                   (double)start_stat.bytes/(1024*1024),
                                   (double)(CLOCK_CONF_SECOND*start_stat.bytes/(1024*1024))/total_time,
                                   (double)(CLOCK_CONF_SECOND*block_bytes/(1024*1024))/intv_time,
                                   start_stat.packet_errs, start_stat.packets_out_of_order
                                   );


                            intv_stat = start_stat;
                            intv_stat.start_time = cur_clock;
                        }

                        fflush(stdout);


                    }

                    last_packet_size = bytes_recvd;
                }

#ifdef _WIN32
                /* �������� ������� ������� ��� ������ */
                if (!err && _kbhit())
                    f_out = 1;
#endif
            }


            bytes_sent = sendto(sock, stop_cmd, strlen( stop_cmd ) + 1, 0,
                                ( struct sockaddr* )&sa, sizeof( struct sockaddr_in ));
            if (bytes_sent < 0) {
                fprintf(stderr, "������ �������� ������� �� ������� �����: %s\n", f_get_last_err_str() );
                if (!err)
                    err = ERR_SEND;
            }
        }

        closesocket( sock );
    }

#ifdef _WIN32
    WSACleanup();
#endif
 
    return err;
}
